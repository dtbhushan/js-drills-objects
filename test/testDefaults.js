const defaults = require("../defaults")

obj = {
    aditya : "python",
    ramakrishna : undefined,
    kaif : "java"
}
defaultProps = {
    aditya : "python",
    ramakrishna : "javascript",
    kaif : "java"
}

result = defaults(obj, defaultProps)
console.log(result)