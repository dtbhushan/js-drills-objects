const mapObject = require("../mapObject")

let obj = {
    firstkey: 2,
    secondkey: 3,
    thirdkey: 4
}

function cb(value) {
    return value * value
}

result = mapObject(obj, cb)
console.log(result)
