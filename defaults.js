function defaults(obj, defaultProps) {

    for (key in obj) {
        if (typeof obj[key] === "undefined") {
            obj[key] = defaultProps[key]
        }
    }
    return obj
}
module.exports = defaults